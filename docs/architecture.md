
ABOUT

HopShuttle is a stop-to-stop passenger transport system. The 'stops' are pre-determined pick up and drop off points, i.e. bus stops.

Passengers enter a 'ride', which is then matched to a vehicle. The HopShuttle service will attempt to put passengers together in a vehicle where convenient.


GLOSSARY

Ride: a passenger request to travel from one stop to another
Stop: geographic point where a vehicle can pick up or drop off, e.g. a bus stop
Place: stop that is available for users to depart/arrive
Duration: estimated travel time in seconds between two stops
Visit: the act a vehicle visiting a stop to collect or deliver passengers
Route: a collection of visits, usually in the order they should be performed
Origin: the first visit in a route - i.e. stop nearest vehicle (no collect/deliver)
Match: when a vehicle is assigned to service a ride
Position: latitude/longitude


SERVER ARCHITECTURE

Stops, durations and vehicles live in the database. These tables are rarely updated.

Rides and ride events are inserted into the database, but are not updated once created. This allows for an immutable record of all rides and all events.

A ride can exist in four states:
  UNMATCHED (created, no events yet)
  MATCHED (vehicle assigned)
  COLLECTED (ride has departed)
  DELIVERED (ride has arrived)

Database 'views' are used to present these ride states separately.

A recurring job runs to assign unmatched rides to vehicles.

Route calculation is expensive, so instead of doing it on every client request, it is generated regularly by the server and the results are stored in memory. Clients then query this via the API.


CLIENT ARCHITECTURE

Client requests are sent to the '/api' route. Each client request is a JSON data map, as is the response.

