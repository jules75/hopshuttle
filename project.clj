(defproject hopshuttle "0.1"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[compojure "1.4.0"]
                 [com.taoensso/tufte "1.0.2"]
                 [clj-time "0.8.0"]
                 [clj-http "2.0.1"]
                 [cljsjs/google-maps "3.18-1"]
                 [org.clojure/clojure "1.7.0"]
                 [org.clojure/data.csv "0.1.3"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/clojurescript "1.9.89"]
                 [org.clojure/math.combinatorics "0.1.1"]
                 [org.clojure/java.jdbc "0.4.2"]
                 [org.clojure/tools.logging "0.3.1"]
                 [mysql/mysql-connector-java "5.1.38"]
                 [ring "1.4.0"]
                 [ring/ring-json "0.4.0"]
                 [bk/ring-gzip "0.1.1"]
                 [hiccup "1.0.5"]
                 [hiccups "0.3.0"]
                 [prismatic/dommy "1.1.0"]
                 [shodan "0.4.2"]
                 [cljs-ajax "0.5.2"]
                 [yesql "0.5.1"]]
  :plugins [[lein-cljsbuild "1.1.1"]
            [lein-marginalia "0.9.0"]
            ;[scorecard "0.1"]
            [lein-ring "0.9.6"]]
  :ring {:handler hopshuttle.core/app
         :port 81}
  :cljsbuild {:builds
              {:app
               {:source-paths ["src-cljs"]
                :compiler
                {:optimizations :simple
                 :output-to "resources/public/js/out/app.js"}}}})
