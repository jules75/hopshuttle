function onCategoryClick(e) {
	$(".chooser.sub li").removeClass("selected");
	$(".chooser.top li").removeClass("selected");
	$(e.target).addClass("selected");
	$(".chooser.sub, section").hide();
	var category = $(e.target).data("child");
	$(".chooser.sub[data-category='" + category + "']").slideDown();
}

// returns "depart" or "arrive"
function containerType(el) {
  var parent = $(el).closest(".chooser_container");
  return parent.hasClass("depart") ? "depart" : "arrive";
}

function onItemClick(e) {
	$(".chooser.sub li").removeClass("selected");
	$(e.target).addClass("selected");
	$("section").hide();
	var itemId = $(e.target).data("item");
  var selector = "." + containerType(e.target) + " section[data-id=" + itemId + "]";
	$(selector).slideDown();
}

function onDepartChoose(e) {
  $(".chooser_container.depart").slideToggle();
  $(".chooser_container.arrive").slideUp();
  e.preventDefault();
}

function onArriveChoose(e) {
  $(".chooser_container.arrive").slideToggle();
  $(".chooser_container.depart").slideUp();
  e.preventDefault();
}

function onPlaceSelect(e) {
  var placeId = $(e.target).data("id");
  var placeType = containerType(e.target);
  var title = $("." + placeType + " .chooser.sub li[data-item=" + placeId + "]").text();
  $("button#" + placeType).text(title);
  $(".chooser_container." + placeType).slideUp();
  $("#" + placeType + "-id").val(placeId);

  // update clojurescript state
  hopshuttle.ride.set_state_val(placeType + "-id", placeId);

  e.preventDefault();
}

$(".chooser.sub, section").hide();
$(".chooser.top li").click(onCategoryClick);
$(".chooser.sub li").click(onItemClick);
$("section button").click(onPlaceSelect);
$("button.chooser#depart").click(onDepartChoose);
$("button.chooser#arrive").click(onArriveChoose);

