/* 	Show/hide footer items on heading click.
*/

function toggle(el) {
	el.style.display = (el.style.display=="none") ? "block" : "none";
}

function onHeadingClick(e) {
	var paragraphs = e.target.parentElement.querySelectorAll("p");
	paragraphs.forEach(toggle);
}

function addClickHandler(el) {
	el.addEventListener("click", onHeadingClick);
	el.style.cursor = "pointer";
	el.click();
}

var headings = document.querySelectorAll("footer .container div h2");
headings.forEach(addClickHandler);
