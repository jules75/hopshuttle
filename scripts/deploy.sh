#!/bin/sh

echo Copying files to server
rsync -azP --exclude=events.log --exclude=resources/public/static/ --exclude=.lein-plugins/ /mnt/d/Hopshuttle/ root@hopshuttle.com.au:/srv/hopshuttle

echo Restarting app on server
ssh root@hopshuttle.com.au /srv/startservers

