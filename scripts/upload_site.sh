#!/bin/sh

rsync -azP --exclude=events.log --exclude=resources/public/static/ --exclude=.lein-plugins/ /mnt/d/Hopshuttle/ root@hopshuttle.com.au:/srv/hopshuttle

