(ns hopshuttle.driver
  (:require-macros [hiccups.core :refer [html]])
  (:require
   [ajax.core :refer [GET POST PUT]]
   [cljs.reader :refer [read-string]]
   [dommy.core :refer-macros [sel sel1] :as d]
   [hiccups.runtime :refer [escape-html]]
   [hopshuttle.ride :refer [api-call]]
   [hopshuttle.storage :as s]
   [shodan.inspection :refer [inspect]]))


; mutable state
(def vehicle-route (atom nil))
(def vehicle-id (atom nil))
(def fetch-route? (atom true))
(def position-report-time-ms (atom))
(def last-reported-position (atom {:lat nil :lng nil}))


(def store (s/atom)) ; persists to localstorage


; see http://blog.altometrics.com/2016/05/protocols-in-clojurescript/
(extend-type js/NodeList
  ISeqable
  (-seq [node-list] (array-seq node-list)))


(defn action-response-handler
  [response]
  (reset! fetch-route? true) ; resume route fetch
  (when (false? (:success response))
    (js/alert (str "ERROR: " (:msg response)))))



(defn distance
  "Metres between two lat/lng positions
  Adapted from http://stackoverflow.com/a/27943/2205882"
  [pos1 pos2]
  (let [lat1 (:lat pos1)
        lng1 (:lng pos1)
        lat2 (:lat pos2)
        lng2 (:lng pos2)
        deg2rad #(* % (.-PI js/Math) 0.005555555555)
        earth-radius 6371
        dlat (deg2rad (- lat2 lat1))
        dlng (deg2rad (- lng2 lng1))
        square #(* % %)
        a (+ (square (.sin js/Math (* dlat 0.5)))
             (* (.cos js/Math (deg2rad lat1)) (.cos js/Math (deg2rad lat2))
                (square (.sin js/Math (* dlng 0.5)))))
        c (* 2 (.atan js/Math (.sqrt js/Math a) (.sqrt js/Math (- 1 a))))]
    (int (* earth-radius c 1000))
    ))


(defn action-ride
  [e]
  (when (js/confirm "Continue?")
    (let [button (.-target e)
          ride-id (js/Number (d/attr button :data-ride-id))
          action (d/attr button :data-action)
          pvkey (get-in @store [:vehicle :keys @vehicle-id])
          payload {:action (.toUpperCase action)
                   :vehicle_id @vehicle-id
                   :pvkey pvkey
                   :ride_id ride-id}]
      (reset! fetch-route? false) ; pause route fetch
      (d/set-attr! button :disabled)
      (api-call PUT "/api/ride" payload action-response-handler)
      )))


(defn fetch-mobile
  [e]
  (let [button (.-target e)
        pvkey (get-in @store [:vehicle :keys @vehicle-id])
        url (str "/api/passenger")
        params {:pvkey pvkey
                :ride_id (js/Number (d/attr button :data-ride-id))
                :vehicle_id  @vehicle-id}]
    (api-call GET url params
              #(.open js/window
                      (str "/call?phone=" (:mobile %)
                           "&msg=" (:matched_ride_count %) " rides for this user")
                      ))))


(defn create-button-listeners
  "Listen for clicks on collect/deliver buttons."
  []
  (doseq [button (sel [:td :button.action])]
    (d/listen! button :click action-ride))
  (doseq [button (sel [:td :button.call])]
    (d/listen! button :click fetch-mobile)))


(defn render-table
  "Render route as table."
  [route]
  (d/set-html!
   (d/sel1 :table)
   (html
    (->>

     (for [[n visit] (map-indexed vector (:visits route))
           :let [ride-id (-> visit :ride :id)
                 action (get {"depart" "collect" "arrive" "deliver"} (:type visit))
                 desc (-> visit :stop :description)
                 button [:button {:class "action" :data-action action :data-ride-id ride-id} desc]]]
       [:tr {:class "next"}
        [:td ride-id]
        [:td (if (zero? n) button desc)]
        [:td [:button {:class "call" :data-ride-id ride-id} "Call"]]
        ])

     (cons [:caption (-> route :vehicle :plate)])
     (cons [:thead [:tr [:td "Ride id"] [:td "Passenger"] [:td "Stop"] [:td "Action"]]])

     )))
  (create-button-listeners))


; render table when route changes
(add-watch vehicle-route nil #(render-table %4))


(defn on-get-route
  [routes]
  (let [mine? #(= @vehicle-id (get-in % [:vehicle :id]))]
    (reset! vehicle-route (first (filter mine? routes)))))


(defn get-route
  []
  (when @fetch-route?
    (api-call GET "/api/zone" {:zone_id 1} on-get-route)))


(defn render-status
  "Show driver how long since position reported."
  []
  (let [n (- (.now js/Date) @position-report-time-ms)
        s (str (int (/ n 1000)) "s")]
    (when (not (js/isNaN n))
      (d/set-text! (sel1 :#status) s))))


(defn on-report-position
  [response]
  (reset! position-report-time-ms (.now js/Date))
  (when (false? (:success response))
    (js/alert (str "ERROR reporting position: " (:msg response)))))


(defn online?
  "True if checkbox is ticked for vehicle to report as 'online'."
  []
  (boolean (.-checked (sel1 :#online))))


(defn report-position?
  "True if checkbox is ticked for vehicle to report position"
  []
  (boolean (.-checked (sel1 :#report))))


(defn report-position
  "Using position stored in form, report position to server."
  []
  (let [lat (-> (sel1 :input#lat) d/value js/Number)
        lng (-> (sel1 :input#lng) d/value js/Number)
        pos {:lat lat :lng lng}
        pvkey (get-in @store [:vehicle :keys @vehicle-id])
        url "/api/position"
        payload {:lat lat
                 :lng lng
                 :online (if (online?) 1 0)
                 :pvkey pvkey
                 :vehicle_id @vehicle-id}]
    (when (not (nil? (:lat @last-reported-position)))
      (d/set-text! (sel1 :#distance) (str (distance pos @last-reported-position) "m")))
    ;(.log js/console (distance pos @last-reported-position))
    (when (report-position?)
      (reset! last-reported-position pos)
      (api-call POST url payload on-report-position))))


(defn on-geo-locate
  "Callback when GPS reports position. Store lat/lng result in form fields."
  [pos]
  (let [coords (.-coords pos)
        lat (.-latitude coords)
        lng (.-longitude coords)]
    (d/set-value! (sel1 :input#lat) lat)
    (d/set-value! (sel1 :input#lng) lng)))


(defn set-vehicle-key
  []
  (let [path [:vehicle :keys @vehicle-id]
        vkey (get-in @store path)
        response (js/prompt "Vehicle key (16 characters)" vkey)]
    (when response
      (swap! store assoc-in path response))))


(defn on-checkbox-click
  [e]
  (.log js/console e)
  (let [f (if (online?) d/add-class! d/remove-class!)]
    (f (sel1 :body) :online)))


;
; Because compiled javascript can be called by multiple
; pages, check which page we're on before initialising.
;
(when (= "/driver" (.-pathname js/location))

  (let [vid (js/Number (last (re-find #"\?id=(\d+)" (.-search js/location))))]

    (d/listen! (sel1 :#vhkey) :click set-vehicle-key)

    (doseq [el (d/sel "input[type=\"checkbox\"]")]
      (d/listen! el :click on-checkbox-click))

    (reset! vehicle-id vid)
    (get-route)
    (js/setInterval render-status 1000)
    (js/setInterval get-route 5000)
    (js/setInterval report-position 5100)

    ; tell GPS to report when position changes
    (.watchPosition
     (.-geolocation js/navigator)
     on-geo-locate
     #(js/alert (str "ERROR reading GPS because: " (.code %)))
     (clj->js {:enableHighAccuracy true}))

    ))

