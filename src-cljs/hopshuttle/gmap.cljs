(ns hopshuttle.gmap
  (:require-macros [hiccups.core :refer [html]])
  (:require
   [dommy.core :refer-macros [sel sel1] :as d]
   [hopshuttle.map :refer [create-map]]
   [hopshuttle.storage :as s]))


(def store (s/atom)) ; persists to localstorage


(def gmap (atom nil))


(defn click-stop
  [e]
  (this-as marker
           (let [id (js/Number (.-id marker))]
             (swap! store update-in [:user :stops] conj id)
             (swap! store update-in [:user :stops] set)
             (js/alert (str "Stop " id " saved to your list"))
             )))


(defn create-marker
  [stop]
  (let [data (.-dataset stop)
        lat (js/Number (.-lat data))
        lng (js/Number (.-lng data))
        desc (.-description data)
        marker-opts {:position (google.maps.LatLng. lat lng) :title desc :map @gmap :id (.-id data)}
        marker (google.maps.Marker. (clj->js marker-opts))]
    (.addListener marker "click" click-stop)
    ))


;
; Because compiled javascript can be called by multiple
; pages, check which page we're on before initialising.
;
(when (= "/map" (.-pathname js/location))

  (reset! gmap (create-map (sel1 :#map)))
  (.setCenter @gmap (clj->js {:lat -37.5 :lng 143.8}))

  (doseq [stop (sel :p.stop)]
    (create-marker stop))

  )

