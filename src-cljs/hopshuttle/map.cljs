(ns hopshuttle.map
  (:require
   [dommy.core :refer-macros [sel sel1] :as d]
   [hopshuttle.config :refer [map-style]]))


(defn create-map
  "Creates Google Map object in target element, returns that object."
  [target]
  (let [zoom 12
        opts {:zoom zoom
              :minZoom 12
              :streetViewControl false
              :mapTypeControl false
              :fullscreenControl false
              :styles (clj->js map-style)}]
    (google.maps.Map. target (clj->js opts))))


(defn map-control
  "Create a control element and display on the map"
  [gmap img-url title handler]
  (let [div (d/add-class! (d/create-element :div) :mapControl)
        url (str "#fff url(" img-url ") no-repeat center")]
    (set! (-> div .-style .-background) url)
    (set! (.-title div) title)
    (d/listen! div :click handler)
    (-> gmap
        .-controls
        (get google.maps.ControlPosition.RIGHT_TOP)
        (.push div))
    div))


(defn create-marker
  "Creates and returns Google Maps marker object."
  ([lat lng title gmap icon-url]
   (create-marker lat lng title gmap icon-url 32 32))
  ([lat lng title gmap icon-url width height]
   (let [opts {:position (google.maps.LatLng. lat lng)
               :map gmap
               :optimized false
               :icon {:url icon-url
                      :scaledSize (google.maps.Size. width height)
                      :anchor (google.maps.Point. 16 16)}
               :title title}]
     (google.maps.Marker. (clj->js opts)))))


(defn create-callout
  "Creates and returns Google Maps callout (info window)."
  [title img-url]
  (google.maps.InfoWindow.
   (clj->js
    {:content
     (str "<p>" title "</p>")})))

