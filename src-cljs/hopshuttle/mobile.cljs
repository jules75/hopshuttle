(ns hopshuttle.mobile)


(defn valid?
  "True if mobile number is valid."
  [mobile]
  (let [digits (map js/Number (re-seq #"\d" mobile))]
    (or
     (and (= [0 4] (take 2 digits)) (= 10 (count digits)))
     (and (= [6 1 4] (take 3 digits)) (= 11 (count digits))))))


(defn normalise
  "Format and return 11 digit string mobile number found in form."
  [s]
  (assert (valid? s))
  (let [digits (map js/Number (re-seq #"\d" s))]
    (apply str
           (if (= 10 (count digits))
             (concat [6 1] (rest digits))
             digits))))

