(ns hopshuttle.popup
  (:require [ajax.core :refer [GET]]
            [dommy.core :refer-macros [sel sel1] :as d]))

(defn- destroy-popups
  []
  (doseq [popup (sel :.popup)]
    (d/remove! popup)))


(defn- make-popup
  [html]
  (let [btn (-> (d/create-element :button)
                (d/set-text! "Close"))
        div (-> (d/create-element :div)
                (d/add-class! :popup)
                (d/set-html! html)
                (d/prepend! btn))]
    (d/listen! btn :click destroy-popups)
    (d/append! (sel1 :body) div)))


(defn create
  [id]
  (GET (str "/places/" id "/index.html")
       {:handler make-popup
        :error-handler #(.error js/console "could not fetch popup")}))

