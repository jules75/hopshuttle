(ns hopshuttle.ride-ui
  "Render HTML for ride page."
  (:require-macros [hiccups.core :refer [html]])
  (:require
   [dommy.core :refer-macros [sel sel1] :as d]
   [hiccups.runtime :refer [render-html render-attr-map]]
   [shodan.inspection :refer [inspect]]))


(defn- attach-absolute-offsets
  "Given visits with offsets, attach absolute sums of those offsets."
  [visits]
  (map #(assoc %1 :absolute-offset %2) visits (reductions + (map :offset visits))))


(defn- mine?
  "True if visit belongs to ride."
  [visit ride]
  (and
   (some #{(:id ride)} (:ride-ids visit))
   (or
    (= (:stop-id visit) (:depart_stop_id ride))
    (= (:stop-id visit) (:arrive_stop_id ride)))))


(defn- combine-visits
  "Combines visits suitable for presentation to user.
  Visits get :absolute-offset and :ride-ids keys."
  [visits]
  (let [partitioned-visits (partition-by :stop-id (attach-absolute-offsets visits))
        ride-ids #(get-in % [:ride :id])
        grouped-ride-ids (map (partial map ride-ids) partitioned-visits)]
    (->> partitioned-visits
         (map first)
         (map #(assoc %2 :ride-ids %1) grouped-ride-ids))))


(defn- route->html
  "Given route, return HTML that describes the visits that vehicle will make."
  [route]
  (when (pos? (count (:visits route)))
    (html
     [:div {:class "route"}
      [:h2 (str "Next stops - " (-> route :vehicle :plate))]
      [:ul
       (for [v (combine-visits (:visits route))
             :let [eta (str (int (/ (:absolute-offset v) 60)) "m")
                   ;cl (str "visit" (if (mine? v (:ride state)) " mine"))
                   cl "visit"
                   title (-> v :stop :description)]]
         [:li {:class cl} title [:span {:class "eta"} eta]])]
      [:p {:class "note"} "Times are estimates only"]
      ])))


(defn instructions
  [state]
  (let [vid (some-> state :ride :events first :vehicle_id)
        visits (some-> state :routes (get vid) :visits)
        ride-id (-> state :ride :id)]
    (->>
     visits
     (filter #(= ride-id (:id (:ride %))))
     (filter #(= :depart (:type %)))
     first
     :stop
     :instructions)))


(defn update-ui
  "Update UI based on state."
  [state]

  ; show/hide cancel button
  (d/toggle! (sel1 :#cancel) (= "EXPIRED" (->> state :ride :events last :status)))

  ; show/hide connection error message
  (d/toggle! (sel1 :#error) (:disconnected? state))

  ; render routes
  (d/clear! (sel1 :#routes))
  (d/set-html!
   (sel1 :#routes)
   (apply str (map route->html (:routes state))))

  ; set vehicle marker positions
  (doseq [vehicle (map :vehicle (:routes state))]
    (let [{:keys [lat lng online]} vehicle
          marker (get-in state [:vh-markers (:id vehicle)])]
      (.setPosition marker (google.maps.LatLng. lat lng))
      (.setOpacity marker (if online 1 0.5))))

  ; show/hide form & ride status
  (let [ride (:ride state)
        statuses {nil {:msg (str "Searching for vehicle (" (:countdown state) ")") :type "searching"}
                  "MATCHED" {:msg "Your ride is on its way!" :type "matched"}
                  "COLLECTED" {:msg "You have been collected" :type "collected"}
                  "EXPIRED" {:msg "Your ride has EXPIRED" :type "expired"}
                  "COMPLETED" {:msg "Your ride is complete" :type "completed"}}
        status (get statuses (->> state :ride :events last :status))
        instr (instructions state)
        msg (if (seq ride) (:msg status))]
    (d/set-attr! (sel1 :#status) :data-event-type (:type status))
    (d/toggle! (sel1 :#status) (seq msg))
    (d/toggle! (sel1 :#hail) (nil? msg))
    (d/toggle! (sel1 :p.instructions) (seq instr))
    (d/set-text! (sel1 :p.instructions) instr)
    (d/set-text! (sel1 [:#status :p]) msg))

  ; show/hide mobile/sms form & hail button
  (let [dep (:depart-id state)
        arr (:arrive-id state)
        stops-selected? (and
                         (nil? (:ride state))
                         (pos? dep)
                         (pos? arr)
                         (not= dep arr))
        sms-code-known? (:sms-code-known? state)]
    (d/toggle! (sel1 :fieldset) stops-selected?)

    (doseq [el (sel "fieldset div:not(:first-child)")]
      (d/toggle! el sms-code-known?))

    (d/toggle! (sel1 :#submit) (and sms-code-known? stops-selected?))))

