(ns hopshuttle.ride
  (:require
   [ajax.core :refer [GET POST PUT]]
   [cljs.reader :refer [read-string]]
   [dommy.core :refer-macros [sel sel1] :as d]
   [shodan.inspection :refer [inspect]]
   [hopshuttle.map :refer [create-map create-marker create-callout map-control]]
   [hopshuttle.mobile :as mob]
   [hopshuttle.popup :as popup]
   [hopshuttle.ride-ui :refer [update-ui]]
   [hopshuttle.storage :as s]))


(def cache-buster (atom 0))
(def store (s/atom))               ; persists to localstorage
(def state (atom {:depart-id nil
                  :arrive-id nil
                  :routes []
                  :ride nil 		   ; user's current ride (if any) and its events
                  :pos-marker nil
                  :vh-markers {}   ; vehicle id as key
                  :vh-callouts {}  ; vehicle id as key
                  :disconnected? false
                  :sms-code-known? false
                  :gmap nil}))


(defn- ancestor
  "Given element, return first ancestor (not necessarily parent) with tag."
  [tag el]
  (if (and (not (nil? el)) (not= tag (.-tagName el)))
    (recur tag (.-parentNode el))
    el))


; thanks to http://stackoverflow.com/a/3249777/2205882
(defn in?
  "true if coll contains elm"
  [coll elm]
  (some #(= elm %) coll))


(defn on-timeout
  "Called when AJAX call times out"
  [e]
  (swap! state assoc :disconnected? true)
  (.error js/console (clj->js e)))


(defn api-call
  "Call server API"
  ([method url payload on-success] (api-call method url payload on-success 0))
  ([method url payload on-success interval-ms]
   (let [err-fn #(let [msg (->> % :response :msg)]
                   (if (empty? msg)
                     (swap! state assoc :disconnected? true)
                     (js/alert (str "ERROR: " msg))))
         api-fn #(method url
                         {:params (assoc payload :r @cache-buster)
                          :format :raw
                          :response-format :json
                          :keywords? true
                          :error-handler err-fn
                          :handler (fn [response]
                                     (do
                                       (swap! cache-buster inc)
                                       (swap! state assoc :disconnected? false)
                                       (on-success response)))})]
     (if (pos? interval-ms) (js/setInterval api-fn interval-ms))
     (api-fn))))


(defn on-get-ride
  "Callback, store ride and its events in state. Remove ride if complete."
  [ride]
  (when (= "DELIVERED" (->> @state :ride :events last :status))
    (swap! store dissoc :ride)
    (.reload js/location))
  (swap! state assoc :ride ride))


(defn on-add-ride
  "Save ride id to storage, start polling for ride details."
  [response]
  (if-let [id (:ride_id response)]
    (do
      (swap! state assoc :countdown 60)
      (js/setInterval
       #(swap! state update :countdown (fn [n] (if (pos? n) (dec n) n)))
       1000)
      (if (= "?provider" (.-search js/location))
        (.reload js/location)
        (do
          (swap! store assoc-in [:ride :id] id)
          (api-call GET "/api/ride" {:ride_id id} on-get-ride 2000)
          ))
      )
    (js/alert "Could not add ride, please check your mobile number and PIN.")))


(defn on-get-visible-stops
  "Create stop markers and callouts."
  [stops]
  (doseq [{:keys [id lat lng description]} stops
          :let [marker (create-marker lat lng description (:gmap @state) "/img/stop.svg")
                url (str "/img/stops/" id ".jpg")
                callout (create-callout description url)]]
    (.addListener marker "click" #(.open callout (:gmap @state) marker))
    ;;POPUPS;;(.addListener marker "click" #(popup/create id))
    ))


(defn add-ride
  [e]
  (cond

   (empty? (:routes @state))
   (js/alert "No vehicles available at this time")

   ; iOS doesn't support 'required' attribute (boo, hiss), have to check manually
   (not (.-checked (sel1 :#verify_check)))
   (js/alert "Please check the box to continue")

   :else
   (let [mobile (mob/normalise (d/value (sel1 :#mobile)))
         sms_code (d/value (sel1 :#sms_code))]
     (swap! store assoc-in [:user :mobile] mobile)
     (swap! store assoc-in [:user :sms_code] sms_code)
     (api-call POST "/api/ride" {:mobile mobile
                                 :sms_code sms_code
                                 :depart_stop_id (:depart-id @state)
                                 :arrive_stop_id (:arrive-id @state)}
               on-add-ride)))
  (.preventDefault e))


(defn cancel-ride
  [e]
  (swap! store dissoc :ride)
  (.reload js/location))


(defn request-sms
  "Ask server to send code via SMS."
  [e]
  (let [mobile (d/value (sel1 :#mobile))]
    (if (mob/valid? mobile)
      (api-call POST "/api/sms" {:mobile (mob/normalise mobile)}
                #(do
                   (swap! state assoc :sms-code-known? true)
                   (js/alert (str "PIN has been sent to " mobile))))
      (js/alert "Your mobile phone number doesn't look right")))
  (.preventDefault e))


(defn centre-map
  []
  (let [ballarat {:lat -37.561748 :lng 143.857927}]
    (.setCenter (:gmap @state) (clj->js ballarat))))


(defn toggle-fullscreen
  "Toggle map fullscreen mode."
  [e]
  (d/toggle-class! (sel1 :#map) :fullscreen)
  (.trigger google.maps.event (:gmap @state) "resize")
  (centre-map))


(defn auto-store
  "Auto-save fields with class 'store' to localstorage."
  []
  (let [f (fn [e]
            (let [t (.-target e)]
              (swap! store assoc-in [:user (keyword (.-id t))] (.-value t))))]
    (doseq [field (d/sel :input.store)]
      (d/listen! field :input f))))


(defn- seconds-ago
  "Return age of jsdate object in seconds."
  [jsdate]
  (int (/ (- (.getTime (js/Date.)) (.getTime jsdate)) 1000)))


(defn- find-vehicle
  "Returns vehicle with given id from current state."
  [id]
  (:vehicle (first (filter #(= id (get-in % [:vehicle :id])) (:routes @state)))))


(defn- on-vehicle-click
  []
  (this-as
   this
   (let [vid (.-vehicle_id this)
         vh (find-vehicle vid)
         img-url (str "/img/vehicles/" (:id vh) ".jpg")
         ;ago (str "<em>(" (seconds-ago (:reported vh)) "s ago)</em>")
         html (str "<p>" (:plate vh) "</p>"
                   (when (not (:online vh)) "<p>Not taking passengers</p>"))
         callout (create-callout html img-url)]
     (when-let [c (get-in @state [:vh-callouts vid])] (.close c))
     (swap! state assoc-in [:vh-callouts vid] callout)
     (.open callout (:gmap @state) (get-in @state [:vh-markers vid])))))


(defn create-vehicle-marker
  [vid plate]
  (let [marker (create-marker 0 0 "" (:gmap @state) (str "/vh.svg?plate=" plate) 76 49)]
    (aset marker "vehicle_id" vid)
    (swap! state assoc-in [:vh-markers vid] marker)
    (.addListener marker "click" on-vehicle-click)))


(defn- pan-to-position
  "Callback to pan map to new position."
  [pos]
  (let [zoom 18
        coords (.-coords pos)
        lat (.-latitude coords)
        lng (.-longitude coords)
        latlng (google.maps.LatLng. lat lng)]
    (if (:pos-marker @state)
      (.setPosition (:pos-marker @state) latlng)
      (swap! state assoc :pos-marker (create-marker lat lng "Your position" (:gmap @state) "/img/position.png")))
    (.panTo (:gmap @state) latlng)
    (.setZoom (:gmap @state) zoom)))


(defn- on-location-button
  [e]
  (js/alert "For best location accuracy, turn on your GPS")
  (let [opts {:enableHighAccuracy true}]
    (.watchPosition (.-geolocation js/navigator) pan-to-position nil (clj->js opts)))
  (.preventDefault e))


(defn close-closeable
  "Close a closeable element, remember the close."
  [e]
  (let [div (ancestor "DIV" (.-target e))]
    (swap! store update :closeables conj (.-id div))
    (.removeChild (.-parentNode div) div)))


(defn set-state-val
  "Convenience fn to modify state from javascript."
  [string-key v]
  (swap! state assoc (keyword string-key) v))


;
; Because compiled javascript can be called by multiple
; pages, check which page we're on before initialising.
;
(when (= "/" (.-pathname js/location))

  ; show content and footer, hide loading message
  (d/show! (sel1 :#content))
  (d/show! (sel1 :footer))
  (d/hide! (sel1 :#loading))

  ; check for existing ride
  (when-let [id (-> @store :ride :id)]
    (api-call GET "/api/ride" {:ride_id id} on-get-ride 2000))

  ; create map and controls
  (swap! state assoc :gmap (create-map (sel1 :#map)))
  (map-control (:gmap @state) "/img/fullscreen.png" "Full screen" toggle-fullscreen)
  ; line below commented out until Chrome secure origin issue sorted out
  ;(map-control (:gmap @state) "/img/location.png" "Your location" on-location-button)
  (centre-map)

  ; create vehicle markers
  (doseq [[vid plate] [[1 "1IP4CH"] [2 "ABC999"]]]
    (create-vehicle-marker vid plate))

  ; create stop markers
  (api-call GET "/api/places" {:zone_id 1} on-get-visible-stops)

  ; populate mobile # and sms code
  (d/set-value! (sel1 :#mobile) (-> @store :user :mobile))
  (d/set-value! (sel1 :#sms_code) (-> @store :user :sms_code))

  ; no need to hide sms code field if already known
  (swap! state assoc :sms-code-known? (-> @store :user :sms_code))

  ; update ui on state change
  (add-watch state nil #(update-ui @state))

  ; listeners
  (d/listen! (sel1 :form) :submit add-ride)
  (d/listen! (sel1 :#cancel) :click cancel-ride)
  (d/listen! (sel1 :#sms) :click request-sms)

  ; get routes on interval
  (api-call GET "/api/zone" {:zone_id 1} #(swap! state assoc :routes %) 2500)

  ; show as-yet-unclosed closeable windows
  (doseq [el (sel [:.closeable])
          :when (not (in? (:closeables @store) (.-id el)))]
    (d/set-style! el :display "block")) ; d/show! doesn't work in this case

  ; listen for closing closeable windows
  (doseq [el (sel [:.closeable :button])]
    (d/listen! el :click close-closeable))

  (auto-store)
  (update-ui @state))

