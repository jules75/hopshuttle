(ns hopshuttle.storage
  "Store and fetch values from localstorage."
  (:refer-clojure :exclude [atom])
  (:require
   [cljs.reader :refer [read-string]]))


(def KEY "hopshuttle")


(defn atom
  "Return an atom that syncs with localstorage.
  Creates localstorage value if not present."
  []
  (when (empty? (.getItem js/localStorage KEY))
    (.setItem js/localStorage KEY (str {})))
  (let [m (read-string (.getItem js/localStorage KEY))
        a (clojure.core/atom m)
        w #(.setItem js/localStorage KEY %4)]
    (add-watch a nil w)
    a))

