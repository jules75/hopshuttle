(ns hopshuttle.api
  (:require [cognitect.transit :as t]
            [compojure.core :refer [routes GET POST PUT]]
            [clj-http.client :refer [post]]
            [hopshuttle.config :refer [DB pushover smsglobal]]
            [hopshuttle.route :refer [first=]]
            [hopshuttle.sms :refer [send-sms]]
            [ring.util.response :refer [response]]
            [yesql.core :refer [defqueries]]))


(defqueries "sql/queries.sql" {:connection DB})


(def active-routes (atom []))



(defn send-notification
  "Sends a notification message to site admin"
  [msg]
  (let [params {:token (:token pushover)
                :user (:user-key pushover)
                :message msg}
        response (post (:uri pushover) {:form-params params})]
    (if (= 200 (:status response))
      "Thank you, message sent"
      "Error sending message")))


(defn- valid-key?
  "True if key for given vehicle (int) matches database.
  NOTE: vid is string - FIX"
  [vid pvkey]
  (if (not (nil? vid))
    (-> {:id vid} get-vehicle-private-key first :pvkey (= pvkey))))


(defn- valid-ride?
  "True if depart/arrive stops differ and are in same zone."
  [depart_stop_id arrive_stop_id]
  (let [stops (get-stops {:ids [depart_stop_id arrive_stop_id]})]
    (if (seq stops)
      (and
       (not= depart_stop_id arrive_stop_id)
       (apply = (map :zone_id stops))))))


(defn- coerce-int
  "Coerce string to int, -1 on error."
  [s]
  (try (Integer/parseInt s) (catch Exception e -1)))



(defn- forbidden
  [body]
  {:status 403
   :headers {}
   :body body})


(defn- bad-response
  [body]
  {:status 400
   :headers {}
   :body body})


(def api-routes

  (routes


   (GET "/api/places" [zone_id]
        (let [id (coerce-int zone_id)
              places (get-places-by-zone {:zone_id id})]
          (response (map #(select-keys % [:id :lat :lng :description :category]) places))
          ))

   (GET "/api/stops" [zone_id]
        (let [id (coerce-int zone_id)
              stops (get-all-stops {:zone_id id})]
          (response (map #(select-keys % [:id :lat :lng :description :title]) stops))
          ))

   (GET "/api/ride" [ride_id]
        (let [id (coerce-int ride_id)
              rows (get-ride-events {:id id})
              ride (select-keys (first rows) [:id :depart_stop_id :arrive_stop_id :created])
              events (map #(select-keys % [:vehicle_id :plate :status :event_created]) rows)
              events (if (nil? (:status (first events))) nil events)]
          (response (assoc ride :events events))
          ))

   (GET "/api/passenger" [ride_id vehicle_id pvkey]
        (if (valid-key? vehicle_id pvkey)
          (response (first (get-user-by-ride-id {:ride_id ride_id :vehicle_id vehicle_id})))
          (forbidden {:msg "Authorisation failed"})
          ))


   (POST "/api/position" [vehicle_id pvkey lat lng online]
         (if (valid-key? vehicle_id pvkey)
           (do
             (add-vehicle-coords! {:vehicle_id vehicle_id :lat lat :lng lng :online online})
             (response {:msg "Position recorded"}))
           (forbidden {:msg "Authorisation failed"})
           ))


   (POST "/api/ride" [depart_stop_id arrive_stop_id mobile sms_code]
         (let [user (first (get-user {:mobile mobile :sms_code sms_code}))]

           (cond

            (nil? user)
            (do
              (Thread/sleep 5000) ; frustrate key guessing
              (bad-response {:msg "Mobile # and/or SMS code incorrect"}))


            (not (valid-ride? depart_stop_id arrive_stop_id))
            (bad-response {:msg "Invalid depart_stop_id and/or arrive_stop_id"})

            :else
            (let [r (add-ride<!
                     (conj
                      {:depart_stop_id depart_stop_id :arrive_stop_id arrive_stop_id}
                      {:user_id (:id user)}))]
              (send-notification "New ride added")
              (response {:ride_id (:generated_key r)}))

            )))


   (PUT "/api/ride" [ride_id vehicle_id pvkey action]
        (let [ride_id (Integer/parseInt ride_id)
              dbf (get {"COLLECT" get-matched-rides "DELIVER" get-collected-rides}
                       action (constantly {}))
              ride (first= ride_id :id (dbf))]
          (cond

           (nil? (some #{action} #{"COLLECT" "DELIVER"}))
           (bad-response {:msg "Action must be COLLECT or DELIVER"})

           (not (valid-key? vehicle_id pvkey))
           (forbidden {:msg "Authorisation failed"})

           (empty? ride)
           (bad-response {:msg "No candidate ride found"})

           (= "COLLECT" action)
           (do
             (collect-ride! {:vehicle_id vehicle_id :ride_id ride_id})
             (response {:msg "Ride marked as collected"}))

           (= "DELIVER" action)
           (do
             (deliver-ride! {:vehicle_id vehicle_id :ride_id ride_id})
             (response {:msg "Ride marked as delivered"}))

           :else
           (bad-response {:msg "Unknown error"})

           )))


   (POST "/api/sms" [mobile]
         (let [user (first (get-sms-code {:mobile mobile}))
               rand-code (+ 10000 (rand-int 90000))
               code (if user (:sms_code user) rand-code)]
           (if (re-find #"^61\d{9}$" mobile)
             (do
               (when (nil? user) (add-user! {:mobile mobile :sms_code code}))
               (if (send-sms mobile (str code " is your PIN") smsglobal)
                 (response {:msg "SMS sent"})
                 (bad-response {:msg "Error sending SMS, perhaps too many requests. Try again in 30 seconds."})))
             (bad-response {:msg "Mobile number must be in 61xxxxxxxxx format"})
             )))


   ; zone_id is ignored for now, always returns zone 1
   (GET "/api/zone" [zone_id]
        (response @active-routes))


   ))


