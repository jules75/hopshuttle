(ns hopshuttle.calc
  "Calculate vehicle route."
  (:require [hopshuttle.config :refer [DB]]
            [hopshuttle.route :refer [calc-route]]
            [yesql.core :refer [defqueries]]))


(defqueries "sql/queries.sql" {:connection DB})



; calculate vehicle's route, returns JSON data map.
; NOTE: this is slow, as it recalculates the route each time
; For speed, fetch text file at /static/routes/
(defn single-route
  [vehicle_id]
  (let [start (System/currentTimeMillis)
        current-rides (concat (get-collected-rides) (get-matched-rides))
        rides (filter #(= (:vehicle_id %) vehicle_id) current-rides)
        vehicle (first (get-vehicle-by-id {:id vehicle_id}))
        {:keys [lat lng]} vehicle
        vh-stop (first (get-nearest-stop {:lat lat :lng lng :zone_id 1}))
        f #(select-keys % [:depart_stop_id :arrive_stop_id])
        stop-ids (cons (:id vh-stop) (mapcat (comp vals f) current-rides))
        stops (when (seq stop-ids) (get-stops {:ids stop-ids}))
        durations (get-durations {:ids stop-ids})
        visits (calc-route rides stops durations vh-stop)
        finish (System/currentTimeMillis)]
    {:vehicle vehicle
     :meta {:created finish :cost (- finish start)}
     :visits visits}))


