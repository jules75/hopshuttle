(ns hopshuttle.core
  (:require [compojure.core :refer [defroutes routes GET POST]]
            [compojure.route :refer [resources not-found]]
            [compojure.handler :refer [site]]
            [clj-time.coerce :refer [from-sql-time]]
            [clj-time.core :refer [ago seconds before?]]
            [clojure.data.json :as json]
            [clojure.tools.logging :as log]
            [hopshuttle.api :refer [active-routes api-routes send-notification]]
            [hopshuttle.calc :refer [single-route]]
            [hopshuttle.config :refer [DB]]
            [hopshuttle.route :refer [best-match total-duration]]
            [hopshuttle.pages.call :as call]
            [hopshuttle.pages.device :as device]
            [hopshuttle.pages.driver :as driver]
            [hopshuttle.pages.gmap :as gmap]
            [hopshuttle.pages.ride :as ride]
            [hopshuttle.pages.say :as say]
            [hopshuttle.pages.admin.rides :as admin-rides]
            [hopshuttle.pages.admin.ride :as admin-ride]
            [hopshuttle.pages.admin.zone :as admin-zone]
            [hopshuttle.pages.admin.zones :as admin-zones]
            [hopshuttle.svg :refer [wrap-svg vehicle-icon]]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.middleware.gzip :refer [wrap-gzip]]
            [yesql.core :refer [defqueries]])
  (:import [java.io ByteArrayOutputStream]))


(defqueries "sql/queries.sql" {:connection DB})
(defqueries "sql/admin.sql" {:connection DB})


(def prt #(do (log/info %) %))


;(defn save
;  "Save request to database for logging, returns request unchanged."
;  [rq]
;  (let [payload (-> rq :params :payload json->clj)
;        id (:generated_key
;            (add-api-call<!
;             {:ip (or "UNKNOWN" (-> rq :headers (get "x-forwarded-for"))) ; proxy safe
;              :agent (-> rq :headers (get "user-agent"))}))]
;    (when (= :request_sms (:action payload))
;      (add-api-request-sms! {:api_call_id id :number (:mobile payload)}))
;    rq))


(defroutes app-routes

  (GET "/vh.svg" [plate] (vehicle-icon plate))
  (GET "/call" [phone msg] (call/content phone msg))
  (GET "/say" rq (say/content))
  (GET "/driver" rq (driver/content))

  (GET "/device" rq (device/content (-> rq :headers (get "user-agent"))))
  (POST "/device" rq
        (let [agent (get-in rq [:form-params "agent"])
              report (get-in rq [:form-params "user_report"])
              msg (str "User agent:\n\n" agent "\n\nUser report:\n\n" report)]
          (send-notification msg)))

  (GET "/" rq (ride/content (get-places-by-zone {:zone_id 1}) rq))
  (GET "/map" rq (gmap/content (get-all-stops {:zone_id 1})))

  ;(GET "/api" rq (-> rq save :params :payload json->clj prt rq-handler))

  (resources "/")
  (not-found "Page not found"))


(defroutes admin-routes

  (GET "/admin/ride/:id{[0-9]+}" [id] (admin-ride/content (get-ride-events {:id id})))

  (GET "/admin/zone/:id{[0-9]+}" [id]
       (admin-zone/content
        id
        (get-stops-by-zone {:zone_id id})
        (get-durations-by-zone {:zone_id id})
        ))

  (GET "/admin/zones" [] (admin-zones/content (map :zone_id (get-zone-ids))))

  (GET "/admin/rides" rq
       (admin-rides/content
        (let [user_id (some-> rq :query-params (get "user_id") Integer/parseInt)
              rides (get-all-rides)]
          (if user_id
            (filter #(= user_id (:user_id %)) rides)
            rides))))
  )


(def app (-> (routes api-routes admin-routes app-routes) wrap-json-response wrap-gzip wrap-svg site))


(defn match-one-ride
  "Match one ride to a vehicle if possible."
  []
  (let [ride (first (get-unmatched-rides))
        vehicles (for [v (get-vehicles)]
                   (->> (single-route (:id v))
                        :visits
                        total-duration
                        (assoc v :total-duration)))
        vid (best-match ride vehicles)]
    (when (and ride vid)
      (match-vehicle-ride<! {:vehicle_id vid :ride_id (:id ride)}))))


(defn- old?
  "True if ride is 'old' and should be expired."
  [ride]
  (before? (-> ride :ride_created from-sql-time) (-> 60 seconds ago)))


(defn expire-old-rides
  []
  (doseq [ride (filter old? (get-unmatched-rides))]
    (expire-ride! {:ride_id (:id ride)})
    (log/info (format "Ride %d has expired" (:id ride)))))


(when-not *compile-files*
  (future
    (loop []

      ; calculate routes and store in 'routes' atom
      ; only currently active vehicles are included
      (reset! active-routes (map #(single-route (:id %)) (get-vehicles)))

      (expire-old-rides)
      (match-one-ride)
      (Thread/sleep 500)
      (recur))))

