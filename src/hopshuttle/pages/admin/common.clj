(ns hopshuttle.pages.admin.common)


(defn nav
  []
  [:nav
   [:ul
    [:li [:a {:href "/admin/rides"} "Rides"]]
    [:li [:a {:href "/admin/zones"} "Zones"]]
    ]])

