(ns hopshuttle.pages.admin.ride
  (:require [hiccup.page :refer [html5]]
            [hopshuttle.pages.common :refer [head]]
            [hopshuttle.pages.admin.common :refer [nav]]))


(defn content
  [ride-events]
  (html5
   (head "admin")
   (nav)
   [:body

    [:h2 "Ride"]
    [:table
     [:tr [:td "Depart stop"] [:td (:depart_stop_id (first ride-events))]]
     [:tr [:td "Arrive stop"] [:td (:arrive_stop_id (first ride-events))]]
     [:tr [:td "Vehicle"] [:td (:plate (first ride-events))]]]

    [:h2 "Events"]
    [:table
     (for [event ride-events]
       [:tr [:td (:status event)] [:td (:event_created event)]]
       )]

    ]))

