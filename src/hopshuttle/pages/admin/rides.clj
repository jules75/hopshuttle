(ns hopshuttle.pages.admin.rides
  (:require [clj-time.coerce :refer [from-sql-time]]
            [clj-time.core :refer [year month day]]
            [hiccup.page :refer [html5]]
            [hopshuttle.pages.common :refer [head]]
            [hopshuttle.pages.admin.common :refer [nav]]))


(def extract-date (comp (juxt year month day) from-sql-time :created))


(defn content
  [rides]
  (html5
   (head "admin")
   (nav)
   [:body

    (for [[date ride-group] (->> rides (group-by extract-date) (sort-by key) reverse)]

      [:div
       [:h2 (interpose "/" date)]
       [:table
        [:thead
         [:tr
          [:td "Depart"]
          [:td "Arrive"]
          [:td "User"]
          [:td "Created"]
          [:td "Actions"]]]

        (for [ride ride-group]
          [:tr
           [:td (:depart_stop_desc ride)]
           [:td (:arrive_stop_desc ride)]
           [:td [:a {:href (str "/admin/rides?user_id=" (:user_id ride))} (:user_id ride)]]
           [:td (:created ride)]
           [:td [:a {:href (str "/admin/ride/" (:id ride))} "View"]]])

        ]])]))

