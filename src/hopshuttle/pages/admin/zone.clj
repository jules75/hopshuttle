(ns hopshuttle.pages.admin.zone
  (:require [hiccup.page :refer [html5]]
            [hopshuttle.pages.common :refer [head]]
            [hopshuttle.pages.admin.common :refer [nav]]
            [hopshuttle.server :refer [missing-durations]]))


(defn content
  [id stops durations]
  (html5
   (head "admin")
   (nav)
   [:body
    [:h2 (str "Zone " id)]
    (let [c (count stops)]
      [:table
       [:tr
        [:td "Stops"]
        [:td c]]
       [:tr
        [:td "Durations found"]
        [:td (count durations)]]
       [:tr
        [:td "Durations expected"]
        [:td (/ (* c (dec c)) 2)]]
       ])
    [:h2 "Missing durations"]
    [:ul
     (for [pair (missing-durations stops durations)]
       [:li (str pair)]
       )]
    ]))

