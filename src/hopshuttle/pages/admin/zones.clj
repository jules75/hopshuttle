(ns hopshuttle.pages.admin.zones
  (:require [hiccup.page :refer [html5]]
            [hopshuttle.pages.common :refer [head]]
            [hopshuttle.pages.admin.common :refer [nav]]))


(defn content
  [zone-ids]
  (html5
   (head "admin")
   (nav)
   [:body
    [:ul
     (for [id zone-ids]
       [:li [:a {:href (str "/admin/zone/" id)} id]])
     ]]))

