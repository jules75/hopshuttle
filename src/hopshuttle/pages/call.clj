(ns hopshuttle.pages.call
  (:require [hiccup.page :refer [html5]]
            [hiccup.util :refer [escape-html]]
            [hopshuttle.pages.common :refer [head ymd]]
            ))


(defn content
  [phone msg]
  (html5
   (head "call")
   [:body
    [:p [:a {:href (str "tel:1831" phone)} (str "Call (1831)" phone)]]
    [:p msg]
    [:button {:onclick "window.close();"} "Close"]
    ]))

