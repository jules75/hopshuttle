(ns hopshuttle.pages.common
  (:require [clojure.string :refer [join]]
            [clj-time.core :refer [year month day]]
            [clj-time.local :refer [local-now]]
            [hiccup.page :refer [include-css]]
            [hiccup.util :refer [escape-html]]
            ))


(defn ymd
  "Returns vector of 3 values, current year, month and day (local)."
  []
  ((juxt year month day) (local-now)))


(defn head
  "Returns page HTML head."
  ([pg]
   (head pg pg))
  ([pg title]
   [:head
    [:title (str "hopshuttle: " (escape-html title))]
    (include-css (str "/css/style.css?" (join (ymd))))
    (include-css (str "/css/" pg ".css?" (join (ymd))))
    [:link {:href "https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed"
            :rel "stylesheet" :type "text/css"}]
    [:link {:href "https://fonts.googleapis.com/icon?family=Material+Icons"
            :rel "stylesheet"}]
    [:meta {:name "viewport" :content "initial-scale=1.0, maximum-scale=1.0"}]
    [:script {:src "https://maps.googleapis.com/maps/api/js?key=AIzaSyBOQ-E81Y3xbn8WWj2-jAJzjD7aM88T11s"}]
    [:script {:src "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"}]
    ]))

