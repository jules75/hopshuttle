(ns hopshuttle.pages.device
  (:require [hiccup.page :refer [html5]]
            [hiccup.util :refer [escape-html]]
            [hopshuttle.pages.common :refer [head ymd]]
            ))


(defn content
  [user-agent]
  (html5
   (head "device")
   [:body
    [:form {:method "POST"}
     [:input {:name "agent" :type "hidden" :value user-agent}]
     [:textarea {:name :user_report :placeholder "Describe your problem here, include your phone number if you want to be called (optional)"}]
     [:button "Submit bug report to Hopshuttle"]]
    [:p [:a {:href "/"} "Return to Hopshuttle"]]
    ]))

