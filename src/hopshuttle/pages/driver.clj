(ns hopshuttle.pages.driver
  (:require [hiccup.page :refer [html5]]
            [hiccup.util :refer [escape-html]]
            [hopshuttle.pages.common :refer [head ymd]]
            ))


(defn content
  []
  (html5
   (head "driver")
   [:body
    [:div#content
     [:div {:id "options"}
      [:form
       [:p {:style "display:none;"}
        [:input {:type "text" :id "lat"}]
        [:br]
        [:input {:type "text" :id "lng"}]
        ]]]
     [:table]
     [:ul {:id "status-bar"}
      [:li [:button {:id "vhkey"} "key"]]
      [:li {:id "status"} "-"]
      [:li {:id "distance"} "-"]
      [:li [:input {:type "checkbox" :id "report"}]]
      [:li [:input {:type "checkbox" :id "online"}]]
      ]
     ]
    [:script {:src (str "/js/out/app.js?" (apply str (ymd)))}]
    ]))

