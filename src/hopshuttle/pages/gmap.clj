(ns hopshuttle.pages.gmap
  (:require [clojure.string :refer [lower-case]]
            [hiccup.page :refer [html5 include-css]]
            [hiccup.util :refer [escape-html]]
            [hopshuttle.pages.common :refer [head ymd]]
            ))

(defn content
  [stops]
  (html5
   {:lang "en"}
   (head "map" "Ballarat tourist shuttle")
   [:body

    (for [stop stops]
      [:p {:class "stop" :style "display:none;"
       :data-id (:id stop)
       :data-lat (:lat stop)
       :data-lng (:lng stop)
       :data-description (:description stop)}
       ]
      )

    [:div {:id "map"}]

    [:script {:src "/js/out/app.js"}]

    ]))

