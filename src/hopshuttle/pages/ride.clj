(ns hopshuttle.pages.ride
  (:require [clojure.string :refer [lower-case]]
            [hiccup.page :refer [html5 include-css]]
            [hiccup.util :refer [escape-html url-encode]]
            [hopshuttle.pages.common :refer [head ymd]]
            ))


(defn chooser
  "Returns multi-level place chooser widget.
  Target is string 'depart' or 'arrive'."
  [target stops]
  (let [grouped-stops (group-by :category stops)]
    [:div {:class (str "chooser_container", " ", target) :style "display:none;"}

     [:ul {:class "chooser top"}
      (for [[category _] grouped-stops]
        [:li {:data-child (url-encode category)} category]
        )]

     (for [[category grp] grouped-stops]
       [:ul {:class "chooser sub" :data-category (url-encode category)}
        (for [stop grp]
          [:li {:data-item (:id stop)} (:description stop)]
          )
        ]
       )

     (for [stop stops]
       (slurp (str "resources/public/chooser/" (:id stop) "/index.html"))
       )

     ]
    )
  )


(def google-analytics
  "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-1668443-17', 'auto');
  ga('send', 'pageview');")


(def IE "<!--[if IE]><h1 style='color:red'>This browser is too old and is not supported.
  Please consider a modern browser like
  <a href='http://google.com/chrome'>Google Chrome</a>.</h1><![endif]-->")


(defn content
  [stops rq]
  (html5
   {:lang "en"}
   (head "ride" "Ballarat tourist shuttle")
   (include-css "/css/chooser.css")
   [:body

    (str IE)

    [:nav
     [:img {:src "/img/logo.svg"}]]

    [:div {:id "loading"}
     [:h2 "Loading&hellip;"]
     [:p "Stuck loading?" [:a {:href "/device"} "Tell us"] " so we can fix it!"]]

    [:noscript "Javascript is disabled"]

    [:div {:id "content" :style "display:none;"}

     [:div {:class "container"}

      ;      [:div {:id "about" :class "closeable"}
      ;       [:button [:i {:class "material-icons"} "clear"]]
      ;       [:p "Launching Saturday 31 December"]]

      [:div {:id "routes"}]

      [:div {:id "hail"}

       [:form

        [:div
         [:input {:type "hidden" :id "depart-id"}]
         [:label "Starting point"]
         [:button {:class "chooser" :id "depart"} "&nbsp;"]
         (chooser "depart" stops)

         [:input {:type "hidden" :id "arrive-id"}]
         [:label "Destination"]
         [:button {:class "chooser" :id "arrive"} "&nbsp;"]
         (chooser "arrive" stops)]

        [:fieldset
         [:div
          [:label {:for "mobile"} "Your mobile number"]
          [:input {:type "tel" :id "mobile" :class "store" :required "required" :autocomplete "off"}]
          [:button {:id "sms"} "Request PIN"]]
         [:div
          [:label {:for "sms_code"} "Your 5 digit PIN"]
          [:input {:type "tel" :id "sms_code" :class "store" :required "required" :autocomplete "off"}]]
         [:div {:id "verify"}
          [:input {:type "checkbox" :id "verify_check" :required "required"}]
          [:label {:for "verify_check"} "I am ready to travel right now"]]
         ]

        [:input {:type "submit" :id "submit" :value "Hail ride now" :style "display:none;"}]
        ]]

      [:div {:id "status"}
       [:p {:class "pulse"}]
       [:p {:class "instructions"}]
       [:button {:id "cancel"} "Start again"]]

      [:h2 "Tracker"]
      [:div {:id :map}]

      ]]

    [:footer
     [:div {:class "container"}

      [:div
       [:h2 "Frequently Asked Questions"]

       [:p {:class "question"} "What is Hopshuttle?"]
       [:p "Hopshuttle is a free shuttle bus to help tourists and visitors get around Ballarat without a car."]

       [:p {:class "question"} "When does it operate?"]
       [:p "Hopshuttle operates between 10am and 5pm every Saturday and Sunday."]

       [:p {:class "question"} "What does it cost?"]
       [:p "Nothing, Hopshuttle is free."]

       [:p {:class "question"} "Where is the timetable?"]
       [:p "There is no timetable. Hopshuttle is an on demand service that comes when you hail it."]

       [:p {:class "question"} "What number do I call for a ride?"]
       [:p "Hopshuttle can only be hailed via this web site. While you will probably hail with your smartphone, you can also use a desktop or tablet device."]

       [:p {:class "question"} "Can I book in advance?"]
       [:p "No, Hopshuttle is only available on demand during operating hours."]

       [:p {:class "question"} "Is Hopshuttle wheelchair accessible?"]
       [:p "Not currently. Hopshuttle aims to be available to people of all abilities in the future."]

       [:p {:class "question"} "When will my ride arrive?"]
       [:p "Hopshuttle will complete each ride as soon as practical. Watch the online tracker to see where your ride currently is. You can also see a list of upcoming stops with time estimates. The stop list frequently changes, so PLEASE KEEP WATCHING IT."]

       [:p {:class "question"} "My stop moved down the list! What happened?"]
       [:p "Hopshuttle dynamically updates its route to meet demand. For example, it might decide to collect new passengers on the way to collecting you."]
       ]

      [:div
       [:h2 "More"]

       [:p "Hopshuttle is proudly made in Ballarat"]
       [:p [:a {:href "https://www.facebook.com/Hop-Shuttle-1725231811061878"} "Follow us on facebook"]]
       [:p [:a {:href "/docs/alcohol_drug_policy.html"} "Alcohol and drug management policy"]]
       [:p "Email general enquiries to " [:strong "&#106;ulian<span></span>&#64;hops<em></em>huttle&#46;com&#46;au"] ". Ride bookings are not accepted by email."]
       ]

      ]]

    [:p {:id "error" :style "display:none;"} "Reconnecting&hellip;"]

    [:script {:src (str "/js/out/app.js?" (apply str (ymd)))}]
    [:script {:src "js/expander.js"}]
    [:script {:src "js/chooser.js"}]
    [:script google-analytics]

    ]))

