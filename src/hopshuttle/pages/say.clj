(ns hopshuttle.pages.say
  (:require [hiccup.page :refer [html5]]
            [hiccup.util :refer [escape-html]]
            [hopshuttle.pages.common :refer [head ymd]]
            ))


(defn content
  []
  (html5
   (head "say")
   [:body

    [:p "Did Hopshuttle help you have a good visit to Ballarat? Tell somebody!"]

    [:p "There are a few ways you can do this;"]

    [:ol
    [:li "Post to facebook, and mention " [:strong "@hopshuttle" ] " somewhere"]
    [:li "Leave a message on " [:a {:href "http://facebook.com/hopshuttle"} "Hopshuttle's facebook page"]]
    [:li [:a {:href "http://visitballarat.com.au/contact"} "Tell Visit Ballarat"] " (aka the local tourist board)"]
    ]

    [:p "Thanks!"]

    [:p [:a {:href "/"} "Return to Hopshuttle"]]

    ]))

