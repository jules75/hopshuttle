(ns hopshuttle.route
  "Calculate vehicle routes."
  (:require [taoensso.tufte :as tufte :refer (defnp p profiled profile)]
            [clojure.math.combinatorics :refer [permutations]]
            [clj-time.coerce :refer [from-sql-time to-long]]
            [clj-time.core :refer [ago minutes before?]]))


(defn first=
  "Generic fn to get first (f item) that equals v."
  [v f coll]
  (first (filter #(= v (f %)) coll)))


(defn- split
  "Split ride into two visits."
  [ride]
  (let [r (select-keys ride [:id :ride_created])
        v1 {:stop-id (:depart_stop_id ride) :ride r :type :depart}
        v2 {:stop-id (:arrive_stop_id ride) :ride r :type :arrive}]
    (if (= "COLLECTED" (:status ride)) [v2] [v1 v2])))


(defn arrive-last?
  "True if, for collection of visits (WITHOUT origin), arrive always appears after its
  corresponding depart (if it exists). Does not check that every depart has an arrive.
  Uses recursion for performance - this fn is called a lot."
  ([visits] (arrive-last? visits #{}))
  ([visits ids]
   (let [visit (first visits)
         id (-> visit :ride :id)]
     (cond
      (empty? visits) true
      (= :arrive (:type visit)) (recur (rest visits) (conj ids id))
      (get ids id) false
      :else (recur (rest visits) ids)))))


(defn- attach-offsets
  "Using given duration map, attach an 'offset' to each visit."
  [origin-stop durmap visits]
  (let [offsets (->> visits
                     (map (comp :id :stop))
                     (cons (:id origin-stop))
                     (partition 2 1)
                     (map set)
                     (map #(get durmap % 0)))]
    (map #(assoc %1 :offset %2) visits offsets)))


(defn total-duration
  "Returns total duration of visits, i.e. how long (in seconds) route will take."
  [visits]
  (reduce + (map :offset visits)))


(defn- shortest
  "Given two collections of visits, return the visits with the shortest total duration."
  [visits1 visits2]
  (first (sort-by total-duration [visits1 visits2])))


(defn- arrange
  "Arrange visits (WITHOUT origin) into order that satisfies constraints.
  Offsets are also calculated and attached to result."
  [origin-stop durmap visits]
  (let [limit 2500 	; higher is slower
        perms (for [vs (permutations visits)
                    :when (arrive-last? vs)
                    :let [vs (attach-offsets origin-stop durmap vs)]
                    ] vs)]
    (reduce shortest (take limit perms))))


(defn- attach-stop
  "Replace visit's stop-id with full stop details."
  [stops visit]
  (let [stop (first= (:stop-id visit) :id stops)]
    (assoc visit :stop stop)))


(defn- duration-map
  "Turns durations from DB into map with stop id pairs as keys."
  [durations]
  (into {}
        (for [{:keys [stop1_id stop2_id duration]} durations]
          [#{stop1_id stop2_id} duration])))


(defn- old?
  "True if vehicle is too 'old' to accept a ride."
  [vehicle]
  (when-let [t (-> vehicle :reported from-sql-time)]
    (before? t (-> 5 minutes ago))))


(defn best-match
  "Given candidate ride and vehicles, returns id of vehicle best suited, nil if none.
  Vehicles must have :total-duration key to indicate how busy they are."
  [ride vehicles]
  (assert (every? :total-duration vehicles))
  (->> vehicles
       (filter :online)
       (remove old?)
       (sort-by :total-duration)
       first
       :id))


(defn calc-route
  "Calculate route for given rides and stops.
  Rides are assumed to belong to same vehicle, any stops referenced
  in 'rides' must exist in 'stops'."
  [rides stops durations origin-stop]
  (->>
   (mapcat split rides)
   (map (partial attach-stop stops))
   (arrange origin-stop (duration-map durations))))

