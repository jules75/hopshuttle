(ns hopshuttle.server
  "Server side tasks, not part of web server."
  (:require [clojure.set :refer [difference]]
            [clojure.xml :refer [parse]]
            [hopshuttle.config :refer [DB]]
            [yesql.core :refer [defqueries]]))


(defqueries "sql/admin.sql" {:connection DB})


(defn missing-durations
  "Return [id1 id2] of all stop/stop pairs that are missing from durations.
  Stops and durations assumed to be from same zone."
  [stops durations]
  (let [ids (map :id stops)
        stop-pairs (for [a ids b ids :when (not= a b)] [a b])
        duration-pairs (map (juxt :stop1_id :stop2_id) durations)]
    (->>
     (difference
      (set stop-pairs)
      (set duration-pairs)
      (set (map reverse duration-pairs)))
     (filter #(apply < %)))))


(defn- lookup-duration
  "Lookup duration between two points from Google"
  [lat1 lng1 lat2 lng2]
  (let [url (str "https://maps.googleapis.com/maps/api/distancematrix/"
                 "xml?origins=" lat1 "," lng1
                 "&destinations=" lat2 "," lng2 "&sensor=false")]
    (some-> url parse
            :content last
            :content first
            :content second
            :content first
            :content first
            Integer/parseInt)))


(defn- stop-with
  "Given collection of stops, return map of stop with id."
  [id stops]
  (select-keys (first (filter #(= id (:id %)) stops)) [:lat :lng :id]))


(defn- build-sql
  [id1 id2 duration]
  (str "INSERT INTO durations (stop1_id, stop2_id, duration) "
       "VALUES (" id1 "," id2 "," duration ");"))


(defn run
  "Find missing durations for given zone, print out SQL statements to insert them."
  [zone-id]
  (let [stops (get-stops-by-zone {:zone_id zone-id})
        durations (get-durations-by-zone {:zone_id zone-id})
        pause (atom 1e4)]
    (doseq [[id1 id2] (missing-durations stops durations)
            :let [stop1 (stop-with id1 stops)
                  stop2 (stop-with id2 stops)
                  duration (lookup-duration (:lat stop1) (:lng stop1) (:lat stop2) (:lng stop2))]]
      (if (integer? duration)
        (do
          (println (build-sql id1 id2 duration))
          (reset! pause 1e4))
        (do
          (println (str "# waiting " (int (/ @pause 1000)) "s"))
          (Thread/sleep @pause)
          (swap! pause * 2))
        ))))

