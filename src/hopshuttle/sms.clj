(ns hopshuttle.sms
  (:require [clj-http.client :as http]
            [clojure.string :refer [split]]))


; Integer timestamp (in ms) of when the last SMS was sent
; Used to limit outbound rate to 1 SMS every 30 seconds
; Obviously not scalable, will do for early stages
(def sms-timestamp (atom 0))


(defn- allow-sms-send?
  "True if last SMS was sent over 30 seconds ago."
  []
  (< (+ @sms-timestamp (* 30 1000)) (System/currentTimeMillis)))


(defn send-sms
  "Send msg to 11 digit number 'to', returns true on success.
  Returns false if SMS send fails or SMSs being sent too fast."
  [to msg auth]
  {:pre [(re-find #"^61\d{9}$" to)]}
  (let [params {:query-params
                {:action "sendsms"
                 :user (:username auth)
                 :password (:password auth)
                 :from "HopShuttle" ; 11 char max
                 :to to
                 :text msg}}]
    (if (allow-sms-send?)
      (let [response (http/get "https://www.smsglobal.com/http-api.php" params)]
        (reset! sms-timestamp (System/currentTimeMillis))
        (boolean (re-find #"^OK:" (:body response))))
      false)))

