(ns hopshuttle.svg
  "Serve SVG images"
  (:require [clojure.string :refer [replace]]
            [hiccup.util :refer [escape-html]]))


(def icon-svg (slurp "resources/public/img/vh.svg"))

(defn wrap-svg
  "If URI ends with 'svg', serve as SVG."
  [handler]
  (fn [request]
    (let [response (handler request)]
      (if (.endsWith (.toString (:uri request)) "svg")
        (assoc-in response [:headers "Content-Type"] "image/svg+xml")
        response))))

(defn vehicle-icon
  "Returns SVG vehicle icon with given license plate."
  [plate]
  (replace icon-svg #"XXXXXX" (escape-html plate)))

