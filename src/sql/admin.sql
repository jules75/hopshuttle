

-- name: get-zone-ids
SELECT DISTINCT zone_id FROM stops;


-- name: get-stops-by-zone
SELECT * FROM stops
WHERE zone_id = :zone_id;


-- name: get-durations-by-zone
SELECT * FROM durations
WHERE stop1_id IN (SELECT id FROM `stops` WHERE zone_id = :zone_id)
OR stop2_id IN (SELECT id FROM `stops` WHERE zone_id = :zone_id);

