
-- name: get-durations
-- Return all durations for given collection of stop ids
SELECT stop1_id, stop2_id, duration
FROM durations
WHERE stop1_id IN (:ids)
AND stop2_id IN (:ids);


-- name: get-all-stops
-- Return all stops for given zone_id
SELECT id, title, lat, lng, description, zone_id
FROM stops
WHERE zone_id=:zone_id;


-- name: get-stops
-- Return stops with given ids, uses place description where present
SELECT stops.id, title, lat, lng, zone_id,
  IF(places.name IS NULL, stops.description, places.name) as description,
  IF(places.name IS NULL, NULL, places.instructions) as instructions
FROM stops LEFT JOIN places ON stops.id=places.stop_id
WHERE stops.id IN (:ids);


-- name: get-places-by-zone
-- Return all places (stops users can see) by zone
SELECT stop_id AS id, title, lat, lng, name AS description, category, zone_id
FROM places LEFT JOIN stops ON places.stop_id = stops.id
WHERE zone_id = :zone_id
ORDER BY category, description;


-- name: get-nearest-stop
-- Return single stop nearest to lat/lng coordinate
SELECT *, SQRT(POWER(lat-:lat,2) + POWER(lng-:lng,2)) AS dist
FROM stops
WHERE zone_id=:zone_id
ORDER BY dist LIMIT 1;


-- name: get-ride-events
-- Returns ride and its associated events
SELECT rides.id, rides.depart_stop_id, rides.arrive_stop_id,
	rides.created,
    vehicles_rides.vehicle_id,
    vehicles.plate,
    vehicles_rides.status,
    vehicles_rides.created AS event_created
FROM rides
LEFT JOIN vehicles_rides
ON rides.id=vehicles_rides.ride_id
LEFT JOIN vehicles
ON vehicles_rides.vehicle_id=vehicles.id
WHERE rides.id=:id;


-- name: get-vehicle-private-key
SELECT pvkey FROM vehicles WHERE id=:id;


-- name: get-sms-code
SELECT sms_code FROM users WHERE mobile=:mobile;


-- name: get-user
SELECT id, mobile, sms_code FROM users
WHERE mobile=:mobile AND sms_code=:sms_code;


-- name: get-user-by-ride-id
-- Returns user details for given ride id if ride belongs to vehicle
SELECT count(*) as matched_ride_count, users.id, mobile FROM rides LEFT JOIN users
ON rides.user_id = users.id
WHERE user_id =
(SELECT user_id FROM vehicles_rides
LEFT JOIN rides ON vehicles_rides.ride_id = rides.id
WHERE vehicle_id=:vehicle_id AND ride_id=:ride_id AND status="MATCHED");



-- name: get-unmatched-rides
SELECT * FROM rides_unmatched;


-- name: get-matched-rides
SELECT * FROM rides_matched;


-- name: get-collected-rides
SELECT * FROM rides_collected;


-- name: get-all-rides
-- Admin-friendly list of rides
SELECT
  rides.id,
  rides.user_id,
	rides.depart_stop_id,
  rides.arrive_stop_id,
  rides.created,
	s1.description AS depart_stop_desc,
  s2.description AS arrive_stop_desc
FROM rides
  JOIN stops AS s1 ON rides.depart_stop_id = s1.id
  JOIN stops AS s2 ON rides.arrive_stop_id = s2.id
ORDER BY created DESC;


-- name: get-vehicles
-- Returns coordinates of vehicles that have checked-in in the past 5 minutes
-- Query adapted from http://stackoverflow.com/a/1313140 for performance
SELECT DISTINCT a.vehicle_id as id, a.lat, a.lng, a.online, a.created AS reported
FROM vehicles_coords a
  INNER JOIN
  (SELECT vehicle_id, max(created) AS newest FROM vehicles_coords GROUP BY vehicle_id) AS b
  ON a.created = b.newest
  AND a.vehicle_id = b.vehicle_id
WHERE a.created > (NOW() - interval 5 minute);


-- name: get-vehicle-by-id
-- See get-vehicles query
SELECT DISTINCT a.vehicle_id as id, vehicles.plate, a.lat, a.lng, a.online, a.created AS reported
FROM vehicles_coords a
  INNER JOIN
  (SELECT vehicle_id, max(created) AS newest FROM vehicles_coords GROUP BY vehicle_id) AS b
  ON a.created = b.newest
  AND a.vehicle_id = b.vehicle_id
  INNER JOIN vehicles ON a.vehicle_id = vehicles.id
WHERE a.created > (NOW() - interval 5 minute)
AND a.vehicle_id=:id;


-- name: add-ride<!
INSERT INTO rides (depart_stop_id, arrive_stop_id, user_id)
VALUES (:depart_stop_id, :arrive_stop_id, :user_id);


-- name: add-vehicle-coords!
INSERT INTO vehicles_coords (vehicle_id, lat, lng, online)
VALUES (:vehicle_id, :lat, :lng, :online);


-- name: match-vehicle-ride<!
INSERT INTO vehicles_rides (vehicle_id, ride_id, status)
VALUES (:vehicle_id, :ride_id, 'MATCHED');


-- name: collect-ride!
INSERT INTO vehicles_rides (vehicle_id, ride_id, status)
VALUES (:vehicle_id, :ride_id, 'COLLECTED');


-- name: deliver-ride!
INSERT INTO vehicles_rides (vehicle_id, ride_id, status)
VALUES (:vehicle_id, :ride_id, 'DELIVERED');


-- name: expire-ride!
INSERT INTO vehicles_rides (ride_id, status)
VALUES (:ride_id, 'EXPIRED');


-- name: add-user!
INSERT INTO users (mobile, sms_code)
VALUES (:mobile, :sms_code);


-- name: add-api-call<!
INSERT INTO api_call (ip, agent)
VALUES (:ip, :agent);


-- name: add-api-request-sms!
INSERT INTO api_request_sms (api_call_id, number)
VALUES (:api_call_id, :number);



-- name: check-duration-graph-complete
-- Query to quickly count whether there are an appropriate number of entries in the
-- durations table given the contents of the stops table. Does NOT check that there
-- is a duration edge for every stop pair.
SELECT
(SELECT SUM(c) FROM
(SELECT count(id)*(count(id)-1)*0.5 as c FROM stops
group by zone_id) as t)
= (SELECT COUNT(*) FROM durations) AS complete_graph;

