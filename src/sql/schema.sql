-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2016 at 09:46 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: 'smartshuttle'
--

-- --------------------------------------------------------

--
-- Table structure for table 'api_call'
--

CREATE TABLE IF NOT EXISTS api_call (
id int(11) NOT NULL,
  ip varchar(15) NOT NULL,
  agent varchar(200) NOT NULL,
  created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=24065 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table 'api_request_sms'
--

CREATE TABLE IF NOT EXISTS api_request_sms (
  api_call_id int(11) NOT NULL,
  `number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table 'durations'
--

CREATE TABLE IF NOT EXISTS durations (
  stop1_id int(11) DEFAULT '0',
  stop2_id int(11) DEFAULT '0',
  duration int(11) DEFAULT '9999'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table 'places'
--

CREATE TABLE IF NOT EXISTS places (
id int(11) NOT NULL,
  stop_id int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  instructions varchar(250) DEFAULT NULL,
  category varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table 'rides'
--

CREATE TABLE IF NOT EXISTS rides (
id int(11) NOT NULL,
  depart_stop_id int(11) NOT NULL,
  arrive_stop_id int(11) NOT NULL,
  user_id int(11) NOT NULL,
  created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=819 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Stand-in structure for view 'rides_collected'
--
CREATE TABLE IF NOT EXISTS `rides_collected` (
`vehicle_id` int(11)
,`tmp` int(11)
,`id` int(11)
,`user_id` int(11)
,`depart_stop_id` int(11)
,`arrive_stop_id` int(11)
,`status` enum('MATCHED','COLLECTED','DELIVERED','EXPIRED')
,`ride_created` timestamp
,`action_created` timestamp
);
-- --------------------------------------------------------

--
-- Stand-in structure for view 'rides_delivered'
--
CREATE TABLE IF NOT EXISTS `rides_delivered` (
`vehicle_id` int(11)
,`tmp` int(11)
,`id` int(11)
,`user_id` int(11)
,`depart_stop_id` int(11)
,`arrive_stop_id` int(11)
,`status` enum('MATCHED','COLLECTED','DELIVERED','EXPIRED')
,`ride_created` timestamp
,`action_created` timestamp
);
-- --------------------------------------------------------

--
-- Stand-in structure for view 'rides_expired'
--
CREATE TABLE IF NOT EXISTS `rides_expired` (
`vehicle_id` int(11)
,`tmp` int(11)
,`id` int(11)
,`user_id` int(11)
,`depart_stop_id` int(11)
,`arrive_stop_id` int(11)
,`status` enum('MATCHED','COLLECTED','DELIVERED','EXPIRED')
,`ride_created` timestamp
,`action_created` timestamp
);
-- --------------------------------------------------------

--
-- Stand-in structure for view 'rides_matched'
--
CREATE TABLE IF NOT EXISTS `rides_matched` (
`vehicle_id` int(11)
,`tmp` int(11)
,`id` int(11)
,`user_id` int(11)
,`depart_stop_id` int(11)
,`arrive_stop_id` int(11)
,`status` enum('MATCHED','COLLECTED','DELIVERED','EXPIRED')
,`ride_created` timestamp
,`action_created` timestamp
);
-- --------------------------------------------------------

--
-- Stand-in structure for view 'rides_unmatched'
--
CREATE TABLE IF NOT EXISTS `rides_unmatched` (
`id` int(11)
,`user_id` int(11)
,`depart_stop_id` int(11)
,`arrive_stop_id` int(11)
,`UNMATCHED` varchar(9)
,`ride_created` timestamp
);
-- --------------------------------------------------------

--
-- Table structure for table 'stops'
--

CREATE TABLE IF NOT EXISTS stops (
id int(11) NOT NULL,
  title varchar(16) NOT NULL,
  lat double DEFAULT NULL,
  lng double DEFAULT NULL,
  description varchar(100) DEFAULT NULL,
  connected tinyint(1) NOT NULL DEFAULT '0',
  zone_id int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=546 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table 'users'
--

CREATE TABLE IF NOT EXISTS users (
id int(11) NOT NULL,
  mobile varchar(11) NOT NULL,
  sms_code varchar(5) NOT NULL,
  created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table 'vehicles'
--

CREATE TABLE IF NOT EXISTS vehicles (
id int(11) NOT NULL,
  plate varchar(10) NOT NULL,
  pvkey varchar(16) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table 'vehicles_coords'
--

CREATE TABLE IF NOT EXISTS vehicles_coords (
id int(11) NOT NULL,
  vehicle_id int(11) NOT NULL,
  lat double NOT NULL,
  lng double NOT NULL,
  created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  online tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=642120 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table 'vehicles_rides'
--

CREATE TABLE IF NOT EXISTS vehicles_rides (
id int(11) NOT NULL,
  vehicle_id int(11) DEFAULT NULL,
  ride_id int(11) NOT NULL,
  `status` enum('MATCHED','COLLECTED','DELIVERED','EXPIRED') NOT NULL,
  created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1457 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure for view 'rides_collected'
--
DROP TABLE IF EXISTS `rides_collected`;

CREATE ALGORITHM=UNDEFINED DEFINER=root@localhost SQL SECURITY DEFINER VIEW smartshuttle.rides_collected AS select smartshuttle.vehicles_rides.vehicle_id AS vehicle_id,smartshuttle.vehicles_rides.ride_id AS tmp,smartshuttle.vehicles_rides.ride_id AS id,smartshuttle.rides.user_id AS user_id,smartshuttle.rides.depart_stop_id AS depart_stop_id,smartshuttle.rides.arrive_stop_id AS arrive_stop_id,smartshuttle.vehicles_rides.`status` AS `status`,smartshuttle.rides.created AS ride_created,smartshuttle.vehicles_rides.created AS action_created from (smartshuttle.vehicles_rides left join smartshuttle.rides on((smartshuttle.vehicles_rides.ride_id = smartshuttle.rides.id))) where (smartshuttle.vehicles_rides.`status` = 'COLLECTED') having (smartshuttle.vehicles_rides.created = (select smartshuttle.vehicles_rides.created from smartshuttle.vehicles_rides where (smartshuttle.vehicles_rides.ride_id = tmp) order by smartshuttle.vehicles_rides.created desc limit 1));

-- --------------------------------------------------------

--
-- Structure for view 'rides_delivered'
--
DROP TABLE IF EXISTS `rides_delivered`;

CREATE ALGORITHM=UNDEFINED DEFINER=root@localhost SQL SECURITY DEFINER VIEW smartshuttle.rides_delivered AS select smartshuttle.vehicles_rides.vehicle_id AS vehicle_id,smartshuttle.vehicles_rides.ride_id AS tmp,smartshuttle.vehicles_rides.ride_id AS id,smartshuttle.rides.user_id AS user_id,smartshuttle.rides.depart_stop_id AS depart_stop_id,smartshuttle.rides.arrive_stop_id AS arrive_stop_id,smartshuttle.vehicles_rides.`status` AS `status`,smartshuttle.rides.created AS ride_created,smartshuttle.vehicles_rides.created AS action_created from (smartshuttle.vehicles_rides left join smartshuttle.rides on((smartshuttle.vehicles_rides.ride_id = smartshuttle.rides.id))) where (smartshuttle.vehicles_rides.`status` = 'DELIVERED') having (smartshuttle.vehicles_rides.created = (select smartshuttle.vehicles_rides.created from smartshuttle.vehicles_rides where (smartshuttle.vehicles_rides.ride_id = tmp) order by smartshuttle.vehicles_rides.created desc limit 1));

-- --------------------------------------------------------

--
-- Structure for view 'rides_expired'
--
DROP TABLE IF EXISTS `rides_expired`;

CREATE ALGORITHM=UNDEFINED DEFINER=root@localhost SQL SECURITY DEFINER VIEW smartshuttle.rides_expired AS select smartshuttle.vehicles_rides.vehicle_id AS vehicle_id,smartshuttle.vehicles_rides.ride_id AS tmp,smartshuttle.vehicles_rides.ride_id AS id,smartshuttle.rides.user_id AS user_id,smartshuttle.rides.depart_stop_id AS depart_stop_id,smartshuttle.rides.arrive_stop_id AS arrive_stop_id,smartshuttle.vehicles_rides.`status` AS `status`,smartshuttle.rides.created AS ride_created,smartshuttle.vehicles_rides.created AS action_created from (smartshuttle.vehicles_rides left join smartshuttle.rides on((smartshuttle.vehicles_rides.ride_id = smartshuttle.rides.id))) where (smartshuttle.vehicles_rides.`status` = 'EXPIRED') having (smartshuttle.vehicles_rides.created = (select smartshuttle.vehicles_rides.created from smartshuttle.vehicles_rides where (smartshuttle.vehicles_rides.ride_id = tmp) order by smartshuttle.vehicles_rides.created desc limit 1));

-- --------------------------------------------------------

--
-- Structure for view 'rides_matched'
--
DROP TABLE IF EXISTS `rides_matched`;

CREATE ALGORITHM=UNDEFINED DEFINER=root@localhost SQL SECURITY DEFINER VIEW smartshuttle.rides_matched AS select smartshuttle.vehicles_rides.vehicle_id AS vehicle_id,smartshuttle.vehicles_rides.ride_id AS tmp,smartshuttle.vehicles_rides.ride_id AS id,smartshuttle.rides.user_id AS user_id,smartshuttle.rides.depart_stop_id AS depart_stop_id,smartshuttle.rides.arrive_stop_id AS arrive_stop_id,smartshuttle.vehicles_rides.`status` AS `status`,smartshuttle.rides.created AS ride_created,smartshuttle.vehicles_rides.created AS action_created from (smartshuttle.vehicles_rides left join smartshuttle.rides on((smartshuttle.vehicles_rides.ride_id = smartshuttle.rides.id))) where (smartshuttle.vehicles_rides.`status` = 'MATCHED') having (smartshuttle.vehicles_rides.created = (select smartshuttle.vehicles_rides.created from smartshuttle.vehicles_rides where (smartshuttle.vehicles_rides.ride_id = tmp) order by smartshuttle.vehicles_rides.created desc limit 1));

-- --------------------------------------------------------

--
-- Structure for view 'rides_unmatched'
--
DROP TABLE IF EXISTS `rides_unmatched`;

CREATE ALGORITHM=UNDEFINED DEFINER=root@localhost SQL SECURITY DEFINER VIEW smartshuttle.rides_unmatched AS select smartshuttle.rides.id AS id,smartshuttle.rides.user_id AS user_id,smartshuttle.rides.depart_stop_id AS depart_stop_id,smartshuttle.rides.arrive_stop_id AS arrive_stop_id,'UNMATCHED' AS UNMATCHED,smartshuttle.rides.created AS ride_created from (smartshuttle.rides left join smartshuttle.vehicles_rides on((smartshuttle.rides.id = smartshuttle.vehicles_rides.ride_id))) where isnull(smartshuttle.vehicles_rides.`status`) order by smartshuttle.rides.created;

--
-- Indexes for dumped tables
--

--
-- Indexes for table api_call
--
ALTER TABLE api_call
 ADD PRIMARY KEY (id);

--
-- Indexes for table api_request_sms
--
ALTER TABLE api_request_sms
 ADD KEY api_caller_id (api_call_id);

--
-- Indexes for table durations
--
ALTER TABLE durations
 ADD UNIQUE KEY stop1_id (stop1_id,stop2_id);

--
-- Indexes for table places
--
ALTER TABLE places
 ADD PRIMARY KEY (id), ADD KEY stop_id (stop_id);

--
-- Indexes for table rides
--
ALTER TABLE rides
 ADD PRIMARY KEY (id), ADD KEY depart_stop_id (depart_stop_id), ADD KEY arrive_stop_id (arrive_stop_id), ADD KEY user_id (user_id);

--
-- Indexes for table stops
--
ALTER TABLE stops
 ADD PRIMARY KEY (id);

--
-- Indexes for table users
--
ALTER TABLE users
 ADD PRIMARY KEY (id), ADD UNIQUE KEY mobile (mobile);

--
-- Indexes for table vehicles
--
ALTER TABLE vehicles
 ADD PRIMARY KEY (id);

--
-- Indexes for table vehicles_coords
--
ALTER TABLE vehicles_coords
 ADD PRIMARY KEY (id), ADD KEY vehicle_id (vehicle_id), ADD KEY vehicle_id_2 (vehicle_id,created);

--
-- Indexes for table vehicles_rides
--
ALTER TABLE vehicles_rides
 ADD PRIMARY KEY (id), ADD UNIQUE KEY ride_id_2 (ride_id,`status`), ADD UNIQUE KEY ride_id_3 (ride_id,created) COMMENT 'Avoid bug where two events might occur at exactly the same time, hence creating an entry in both the rides_matched and rides_collected views', ADD KEY vehicle_id (vehicle_id), ADD KEY ride_id (ride_id);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table api_call
--
ALTER TABLE api_call
MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24065;
--
-- AUTO_INCREMENT for table places
--
ALTER TABLE places
MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table rides
--
ALTER TABLE rides
MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=819;
--
-- AUTO_INCREMENT for table stops
--
ALTER TABLE stops
MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=546;
--
-- AUTO_INCREMENT for table users
--
ALTER TABLE users
MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table vehicles
--
ALTER TABLE vehicles
MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table vehicles_coords
--
ALTER TABLE vehicles_coords
MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=642120;
--
-- AUTO_INCREMENT for table vehicles_rides
--
ALTER TABLE vehicles_rides
MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1457;
--
-- Constraints for dumped tables
--

--
-- Constraints for table api_request_sms
--
ALTER TABLE api_request_sms
ADD CONSTRAINT api_request_sms_ibfk_1 FOREIGN KEY (api_call_id) REFERENCES api_call (id);

--
-- Constraints for table places
--
ALTER TABLE places
ADD CONSTRAINT places_ibfk_1 FOREIGN KEY (stop_id) REFERENCES `stops` (id);

--
-- Constraints for table rides
--
ALTER TABLE rides
ADD CONSTRAINT rides_ibfk_1 FOREIGN KEY (depart_stop_id) REFERENCES `stops` (id),
ADD CONSTRAINT rides_ibfk_2 FOREIGN KEY (arrive_stop_id) REFERENCES `stops` (id),
ADD CONSTRAINT rides_ibfk_3 FOREIGN KEY (user_id) REFERENCES `users` (id);

--
-- Constraints for table vehicles_coords
--
ALTER TABLE vehicles_coords
ADD CONSTRAINT vehicles_coords_ibfk_1 FOREIGN KEY (vehicle_id) REFERENCES vehicles (id);

--
-- Constraints for table vehicles_rides
--
ALTER TABLE vehicles_rides
ADD CONSTRAINT vehicles_rides_ibfk_1 FOREIGN KEY (vehicle_id) REFERENCES vehicles (id),
ADD CONSTRAINT vehicles_rides_ibfk_2 FOREIGN KEY (ride_id) REFERENCES rides (id);
