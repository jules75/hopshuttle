(ns hopshuttle.core-test
  (:require [clojure.pprint :refer [pprint]]
            [clojure.test :refer :all]
            [cognitect.transit :as t]
            [hopshuttle.config :refer [DB]]
            [hopshuttle.route :refer [best-match calc-route arrive-last? total-duration]]
            [yesql.core :refer [defqueries]])
  (:import [java.sql Timestamp]))


(defqueries "sql/queries.sql" {:connection DB})


(def now (System/currentTimeMillis))

(def r1 {:id 1
         :depart_stop_id 11
         :arrive_stop_id 12
         :status "MATCHED"
         :created (java.sql.Timestamp. (+ now 15000))})

(def r2 {:id 2
         :depart_stop_id 13
         :arrive_stop_id 14
         :status "MATCHED"
         :created (java.sql.Timestamp. now)})

(def r3 {:id 3
         :depart_stop_id 11
         :arrive_stop_id 14
         :status "MATCHED"
         :created (java.sql.Timestamp. now)})

(def r4 {:id 4
         :depart_stop_id 14
         :arrive_stop_id 12
         :status "MATCHED"
         :created (java.sql.Timestamp. now)})

(def r5 {:id 5
         :depart_stop_id 14
         :arrive_stop_id 13
         :status "MATCHED"
         :created (java.sql.Timestamp. now)})

(def r6 {:id 6
         :depart_stop_id 12
         :arrive_stop_id 14
         :status "MATCHED"
         :created (java.sql.Timestamp. now)})

(def stops [{:id 11}
            {:id 12}
            {:id 13}
            {:id 14}])

(def durations [{:stop1_id 11 :stop2_id 12 :duration 300}
                {:stop1_id 11 :stop2_id 13 :duration 330}
                {:stop1_id 11 :stop2_id 14 :duration 360}
                {:stop1_id 12 :stop2_id 13 :duration 390}
                {:stop1_id 12 :stop2_id 14 :duration 420}
                {:stop1_id 13 :stop2_id 14 :duration 450}])

(def vh-stop {:id 14})

(def v1 {:id 1 :plate "ABC123" :total-duration 300 :online false})
(def v2 {:id 2 :plate "DEF456" :total-duration 300 :online true})


(deftest match

  (testing "Ride shouldn't match with offline vehicle"
    (is (= 2 (best-match r1 [v1 v2]))))

  (testing "Least busy vehicle should get ride"
    (let [v1 (assoc v1 :online true)
          v1-visits (calc-route [r1 r2] stops durations vh-stop)
          v2-visits (calc-route [r2] stops durations vh-stop)
          v1 (assoc v1 :total-duration (total-duration v1-visits))
          v2 (assoc v2 :total-duration (total-duration v2-visits))]
      (is (= 2 (best-match r1 [v1 v2])))))

  (testing "No match if no vehicles online"
    (let [v2 (assoc v2 :online false)]
      (is (nil? (best-match r1 [v1 v2])))))

  (testing "Don't match with 'old' vehicles"
    (let [one-hour-ago (- now (* 1000 60 60))
          v1 (-> v1
                 (assoc :reported (java.sql.Timestamp. one-hour-ago))
                 (assoc :online true))
          v2 (assoc v2 :reported (java.sql.Timestamp. now))]
      (is (= 2 (best-match r1 [v1 v2]))))))

(deftest db

  (testing "Are there the correct number of durations for extant stops?"
    (is (= 1 (-> (check-duration-graph-complete) first :complete_graph)))))

(deftest route

  (testing "Depart should occur before arrive (without origin)"
    (is (true? (arrive-last? (rest (calc-route [r1 r2] stops durations vh-stop))))))

  (testing "1 MATCHED + 1 COLLECTED = 3 visits"
    (is (= 3 (count (calc-route [r1 (assoc r2 :status "COLLECTED")] stops durations vh-stop)))))

  (testing "Visits should have integer offsets of 0+"
    (is (every? (partial <= 0) (map :offset (calc-route [r1 r2] stops durations vh-stop)))))

  (testing "Shortest overall duration should be chosen when rides depart together"
    (let [visits (calc-route [(assoc r1 :depart_stop_id 13) r2] stops durations vh-stop)]
      (is (= 1260 (total-duration visits))))))


(deftest arrive-last

  (testing "Testing the arrive-last? fn"
    (is (true? (arrive-last? [{:type :depart :ride {:id 1}}
                              {:type :arrive :ride {:id 1}}
                              {:type :depart :ride {:id 2}}
                              {:type :arrive :ride {:id 2}}])))
    (is (false? (arrive-last? [{:type :depart :ride {:id 1}}
                               {:type :arrive :ride {:id 1}}
                               {:type :arrive :ride {:id 2}}
                               {:type :depart :ride {:id 2}}])))
    ))

(deftest performance

  (testing "Route calculation should complete in < 500ms"
    (let [start (System/currentTimeMillis)]
      (calc-route [r1 r2 r3 r4 r5 r6] stops durations vh-stop)
      (is (> 500 (- (System/currentTimeMillis) start))))))

